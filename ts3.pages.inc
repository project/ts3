<?php
/**
 * @file
 * Functions for non-administration pages of the TeamSpeak 3 module.
 */

/**
 * Viewable page of the TeamSpeak module.
 *
 * @return string
 *   String of HTML.
 */
function ts3_page($id = 'default') {
  drupal_add_css(drupal_get_path('module', 'ts3') . '/includes/ts3view.css');
  include_once(drupal_get_path('module', 'ts3') . '/includes/ts3_func.inc');
  if (!is_numeric($id)) {
    $id = variable_get('ts3_default', t('No Default server setup'));
    if (!is_numeric($id)) {
      return $id;
    }
  }
  $server = ts3_getserver($id);
  $server = $server[$id];
  $ts3 = new Ts3viewer($server['ip'], $server['t_port'], $server['sid'], $server['queryusername'], $server['querypassword']);
  $imagedir = '/' . drupal_get_path('module', 'ts3') . '/images/';
  drupal_add_js('/' . drupal_get_path('module', 'ts3') . '/includes/tsstatus.js');
  $content .= "<div id=\"tsbody\">\n";
  $content .= "  <div id=\"tscont\">\n";
  $row = '';
  $row[] = array('data' => ts3_tree(0, $placeholder, $ts3->clist, $ts3->error, $ts3->sinfo, $ts3->plist,  $server['ip'], $server['port'], $server['cutname'], $server['cutchannel']), 'width' => '50%', 'align' => 'left');

  $rowdata = '  <div style="text-align:center">' . "\n";
  if (!isset($error[0])) {
    $rowdata .= ts3_banner($ts3->banner());
  }
  $rowdata .= '  <h2>' . ts3_rep($ts3->sinfo['virtualserver_name']) . "</h2>\n";
  $rowdata .= '  <h3><a href="ts3server://' . $server['ip'] . '?port=' . $server['port'] . '">' . $server['ip'] . ':' . $server['port'] . '</a></h3>' . "\n";
  if (!isset($error[0])) {
    $rowdata .= ts3_useron($ts3->useron()) . '<br />';
  }
  $rowdata .= ts3_rep($ts3->sinfo['virtualserver_welcomemessage']) . "\n";
  $rowdata .= '  </div>';
  if (!isset($error[0])) {
    $rowdata .= ts3_legend();
    $rowdata .= ts3_stats($ts3->stats());
  }
  $row[] = array('data' => $rowdata, 'width' => '50%', 'valign' => 'top');
  $rows = array('data' => $row); // Note the difference between $row and $rows.
  $content .= theme('table', $tableheader, $rows, array('border' => '0', 'align' => 'center', 'cellspacing' => '20', 'width' => '100'));
  $content .= "  </div>\n";
  $content .= "</div>\n";
  return $content;
}

/**
 * A tree view of the TeamSpeak server.
 *
 * @todo Write documentation for the parameters.
 *
 * @return string
 *   HTML string.
 */
function ts3_tree($id, $placeholder, $clist, $error, $sinfo, $plist, $ip='', $port='', $cutname, $cutchannel) {
  $return = '';
  $imagedir = '/' . drupal_get_path('module', 'ts3') . '/images/';
  $groups = ts3_groups();
  if (!isset($error[0])) {
    foreach ($clist as $key => $var) {
      if ($var['pid'] == $id) {
        $return .= $placeholder;
        if ($var['channel_flag_default'] == 1) {
          $return .= '<div class="tsca" style="float:right;margin-top:2px;"><img src="' . $imagedir . 'home.png" alt="' . ts3_rep($var['channel_topic']) . '" title="' . ts3_rep($var['channel_topic']) . '" /></div>';
        }
        if ($var['channel_flag_password'] == 1) {
          $return .= '<div class="tsca" style="float:right;margin-top:2px"><img src="' . $imagedir . 'schloss.png" alt="' . ts3_rep($var['channel_topic']) . '" title="' . ts3_rep($var['channel_topic']) . '" /></div>';
        }
        $return .= '<div style="padding-left:5px;"><a href="javascript:javascript:tsstatusconnect(\'' . $ip . '\',\'' . $port . '\',\'' . htmlentities($var['channel_name']) . '\')" style="text-decoration:none"><img src="' . $imagedir . 'channel.png" width="14" height="14" alt="' . ts3_rep($var['channel_topic']) . '" title="' . ts3_rep($var['channel_topic']) . '" border="0" />' . truncate_utf8($var['channel_name'], $cutchannel, FALSE, TRUE) . '</a></div>';

        if ($var['total_clients'] >= '1') {
          if ($plist != '') {
            foreach ($plist as $u_key => $u_var) {
              if ($u_var['cid'] == $var['cid']) {
                $p_img = 'player.png';
                if ($u_var['client_input_muted'] == '1') {
                  $p_img = 'mic.png';
                }
                if ($u_var['client_output_muted'] == '1') {
                  $p_img = 'head.png';
                }
                if ($u_var['client_away'] == '1') {
                  $p_img = 'away.png';
                }
                $g_img = '';
                $g_temp = '';
                if (strpos($u_var['client_servergroups'], ',') !== FALSE) {
                  $g_temp = explode(',', $u_var['client_servergroups']);
                }
                else {
                  $g_temp[0] = $u_var['client_servergroups'];
                }
                foreach ($g_temp as $sg_var) {
                  if (isset($groups['sgroup'][$sg_var]['p'])) {
                    $g_img .= '<div class="tsca" style="float:right"><img src="' . $imagedir . '' . $groups['sgroup'][$sg_var]['p'] . '" alt="' . $groups['sgroup'][$sg_var]['n'] . '" /></div>';
                  }
                }
                if (isset($groups['cgroup'][$u_var['client_channel_group_id']]['p'])) {
                  if (isset($groups['cgroup'][$u_var['client_channel_group_id']]['p'])) {
                    $g_img .= '<div class="tsca" style="float:right;"><img src="' . $imagedir . '' . $groups['cgroup'][$u_var['client_channel_group_id']]['p'] . '" alt="' . $groups['cgroup'][$u_var['client_channel_group_id']]['n'] . '" /></div>';
                  }
                }
                $return .= '<div style="position:static;margin-top:2px;margin-left:10px;">' . $placeholder . $g_img . '<div style="padding-left:5px;"><img src="' . $imagedir . '' . $p_img . '" width="12" height="12" alt="Player" /> ' . truncate_utf8($u_var['client_nickname'], $cutname, FALSE, TRUE) . '</div></div>';
              }
            }
          }
        }
        $return .= '<div style="position:static;margin-top:2px;margin-left:10px;">' . ts3_tree($var['cid'], $placeholder, $clist, $error, $sinfo, $plist, $ip, $port, $cutname, $cutchannel) . '</div>';
      }
    }
  }
  else {
    foreach ($error as $var) {
      $return .= $var . '<br />';
    }
  }
  return $return;
}

/**
 * Builds a legend for icons.
 *
 * @return string
 *   String of HTML with legend.
 */
function ts3_legend() {
  $info = ts3_groups();
  $return = '';
  $imagedir = drupal_get_path('module', 'ts3') . '/images/';

  $return .= '<div id="legend"><h3>' . t('Legend') . '</h3>';
  foreach ($info['sgroup'] as $var) {
    $return .= '<div class="tsle">' . theme('image', $imagedir . $var['p'], $var['n'], '', FALSE) . $var['n'] . '</div>';
  }
  foreach ($info['cgroup'] as $var) {
    $return .= '<div class="tsle">' . theme('image', $imagedir . $var['p'], $var['n'], '', FALSE) . $var['n'] . '</div>';
  }
  $return .= '</div>';
  return $return;
}

/**
 * Builds a TS3 statistics block of code.
 *
 * @param array $stats
 *   Array that can be gotten from $ts3viewer->stats().
 *
 * @return string
 *   A HTML string of code.
 */
function ts3_stats($stats) {
  // You can choose what serverinfo will be shown and change the label - Yes=1 / No=0
  $info['platform'] = t('TS3 OS');
  $info['version'] = t('TS3 Version');
  $info['channelsonline'] = t('Channels');
  $info['uptime'] = t('Uptime');
  $info['created'] = t('Online since');
  $return = '<div id="ts3stats"><h3>' . t('Statistics') . '</h3>';
  foreach ($info as $key => $var) {
    $row = '';
    $row[] = array('data' => $var . ':', 'width' => '50%', 'align' => 'left');
    if ($key == 'uptime') {
      $row[] = array('data' => t('@days @hours @minutes', array(
        '@days' => format_plural($stats[$key]['days'], '1 day', '@count days'),
        '@hours' => format_plural($stats[$key]['hours'], '1 hour', '@count hours'),
        '@minutes' => format_plural($stats[$key]['min'], '1 minute', '@count minutes'),
      )), 'width' => '50%', 'align' => 'left');
    }
    else {
      $row[] = array('data' => ts3_rep($stats[$key]), 'width' => '50%', 'align' => 'left');
    }
    $rows[] = array('data' => $row); // Note the difference between $row and $rows
    unset($row);
  }
  $return .= theme('table', $tableheader, $rows, array('border' => '0', 'align' => 'center', 'cellspacing' => '20', 'width' => '100'));
  $return .= '</div>';
  return $return;
}

/**
 * Server Groups
 *
 * To add a group you have to make 2 entries, replace NAME with the name of the
 * group, ID with the Group ID (you find the ID under Server Groups) and PIC
 * with the name of the groupimage. (Copy the image in the images folder. The
 * size from the image should be 16x16 pixels.)
 *
 * $info['sgroup'][ID]['n'] = 'NAME';
 * $info['sgroup'][ID]['p'] = 'PIC';
 *
 * @return array
 *   Array with the groups.
 */
function ts3_groups() {
  $info['sgroup'][6]['n'] = t('Server Admin');
  $info['sgroup'][6]['p'] = 'sa.png';
  // CHANNEL GROUPS
  $info['cgroup'][5]['n'] = t('Channel Admin');
  $info['cgroup'][5]['p'] = 'ca.png';
  $info['cgroup'][6]['n'] = t('Channel Operator');
  $info['cgroup'][6]['p'] = 'co.png';
  return $info;
}

/**
 * Display server banner.
 *
 * @param string $banner
 *   String URL to image file. Can be gotten from $ts3viewer->banner().
 *
 * @return string
 *   String of HTML code
 */
function ts3_banner($banner) {
  $return = '';
  $return .=  theme('image', ts3_rep($banner), t('TS Banner'), '', FALSE);
  return $return;
}

/**
 * Shows number of users online and max users.
 *
 * @param array $users
 *   Keyed array of online and maxusers formed by $ts3viewer->useron().
 * @return string
 *   String of HTML code.
 */
function ts3_useron($users) {
  $return = '';
  $return .= '<div class="useron">' . t('Users online: @online/@maxusers', array(
    '@online' => $users['online'],
    '@maxusers' => $users['maxusers'],
  )) . '</div>';
  return $return;
}

/**
 * Replaces certain predefined characters.
 *
 * @param $var
 *   Message returned from sendcmd().
 *
 * @return string
 *   String with characters replaced.
 */
function ts3_rep($var) {
  $search[] = chr(194);
  $replace[] = '';
  $search[] = chr(183);
  $replace[] = '&#183;';
  $search[] = chr(180);
  $replace[] = '&#180;';
  $search[] = chr(175);
  $replace[] = '&#175;';
  $search[] = '\/';
  $replace[] = '/';
  $search[] = '\s';
  $replace[] = ' ';
  $search[] = '\p';
  $replace[] = '|';
  $search[] = '[URL]';
  $replace[] = '';
  $search[] = '[/URL]';
  $replace[] = '';
  $search[] = '[b]';
  $replace[] = '';
  $search[] = '[/b]';
  $replace[] = '';
  return str_replace($search, $replace, $var);
}
