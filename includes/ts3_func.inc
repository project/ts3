<?php
/**
 * @file
 * TeamSpeak 3 Viewer Module-This is the heart of the project.
 * @author matuck <matuck@matuck.com>
 */

/**
 * Class to communicate with teamspeak 3.
 */
class Ts3viewer {
  #region variable declaration
  private $ip;  //ip of the teamspeak server
  private $t_port;  //the servers query port
  private $sid;  //the teamspeak virtual server id.
  private $queryname;  //the query username
  private $querypass;  //the query password
  public $plist;
  public $clist;
  public $sinfo;
  public $error;
  #endregion

  /**
   * Constructor for the class.
   * Intialize most all class variables and connects to ts server to intialize those variables.
   * @param $ip ip of the teamspeak server
   * @param $t_port the servers query port
   * @param $sid the teamspeak virtual server id.
   * @param $queryname the query username
   * @param $querypass the query password
   */
  public function __construct($ip, $t_port, $sid, $queryname, $querypass) {
    $this->$ip = $ip;
    $this->$t_port = $t_port;
    $this->$sid = $sid;
    $this->$queryname = $queryname;
    $this->$querypass = $querypass;
    $fp = @fsockopen($ip, $t_port, $errno, $errstr, 2);

    if ($fp) {
      $cmd = "login client_login_name=" . $queryname . " client_login_password=" . $querypass . "\n";
      $this->sendCmd($fp, $cmd);
      $cmd = "use sid=" . $sid . "\n";
      if (!($select = $this->sendCmd($fp, $cmd))) {
          $this->error[] = 'Wrong Server ID';
      }
      $cmd = "serverinfo\n";
      if (!($sinfo = $this->sendCmd($fp, $cmd))) {
          $this->error[] = 'No Serverstatus';
      }
      else{
          $this->sinfo = $this->splitInfo($sinfo);
      }
      $cmd = "channellist -topic -flags -voice -limits\n";
      if (!($clist_t = $this->sendCmd($fp, $cmd))) {
          $this->error[] = 'No Channellist';
      }
      else{
          $clist_t = $this->splitInfo2($clist_t);
          foreach ($clist_t as $var) {
              $this->clist[] = $this->splitInfo($var);
          }
      }
      $cmd = "clientlist -uid -away -voice -groups\n";
      if (!($plist_t = $this->sendCmd($fp, $cmd))) {
          $this->error[] = 'No Playerlist';
      }
      else{
          $plist_t = $this->splitInfo2($plist_t);
          foreach ($plist_t as $var) {
              if (strpos($var, 'client_type=0') !== FALSE) {
                  $this->plist[] = $this->splitInfo($var);
              }
          }
          if ($plist != '') {
              foreach ($plist as $key => $var) {
                  $temp = '';
                  if (strpos($var['client_servergroups'], ',') !== FALSE) {
                      $temp = explode(',', $var['client_servergroups']);
                  }
                  else{
                      $temp[0] = $var['client_servergroups'];
                  }
                  $t = '0';
                  foreach ($temp as $t_var) {
                      if ($t_var == '6') {
                          $t = '1';
                      }
                  }
                  if ($t == '1') {
                      $this->plist[$key]['s_admin'] = '1';
                  }
                  else{
                      $this->plist[$key]['s_admin'] = '0';
                  }
              }
              usort($this->plist, array($this, "cmp2"));
              usort($this->plist, array($this, "cmp1"));
          }
      }
      $cmd = "quit\n";
      fputs($fp, $cmd);
    }
    else{
        $this->error[] = 'Can not connect to the server';
    }
  }

  /**
   * Send a command to the teamspeak server.
   * @param $fp the connection to the server
   * @param $cmd the command to send to the server
   * @return $msg a string with the data or false
   */
  private function sendCmd($fp, $cmd) {
    $msg = '';
    fputs($fp, $cmd);
    while (strpos($msg, 'msg=') === FALSE) {
        $msg .= fread($fp, 8096);
    }
    if (!strpos($msg, 'msg=ok')) {
        return FALSE;
    }
    else{
        return $msg;
    }
  }

  /**
   * Break an info string into a usable array
   * @param $info a $msg returned from a sendcmd
   * @return a usable array.
   */
  private function splitInfo($info) {
    $info = trim(str_replace('error id=0 msg=ok', '', $info));
    $info = explode(' ', $info);
    foreach ($info as $var) {
        if (strpos($var, '=')=== FALSE) {
            $return[$var] = '';
        }
        else{
            $return[substr($var, 0, (strpos($var, '=')))] = substr($var, (strpos($var, '=')+1));
        }
    }
    return $return;
  }
  /**
   * Another split info fuction with different delimiter
   * @param $info a $msg returned from a sendcmd
   * @return a usable array.
   */
  private function splitInfo2($info) {
    $info = trim(str_replace('error id=0 msg=ok', '', $info));
    $info = explode('|', $info);
    return $info;
  }

  /**
   * Shows number of users online and max users
   * @return return an array with keys online, maxusers.
   */
  public function useron() {
    return array('online' => ($this->sinfo['virtualserver_clientsonline']-$this->sinfo['virtualserver_queryclientsonline']), 'maxusers' => $this->sinfo['virtualserver_maxclients']);
  }

  /**
   * Gets Server banner url
   * @return string url to server banner
   */
  public function banner() {
    return $this->sinfo['virtualserver_hostbanner_gfx_url'];
  }

  /**
   * Returns server statistics.
   * @return an array with statistics details.
   */
  public function stats() {
    $stats['platform'] = $this->sinfo['virtualserver_platform'];
    $stats['version'] =  $this->sinfo['virtualserver_version'];
    $stats['channelsonline'] = $this->sinfo['virtualserver_channelsonline'];

    $tag = ($this->sinfo['virtualserver_uptime']/1000/60/60/24) %1;
    $std = ($this->sinfo['virtualserver_uptime']/1000/60/60)%24;
    $min = ($this->sinfo['virtualserver_uptime']/1000/60)%60;
    $stats['uptime'] = array( 'days' => $tag, 'hours' => $std, 'min' => $min );
    $stats['created'] = date('d M Y', $this->sinfo['virtualserver_created']);
    return $stats;
  }

  /**
   * Compare strings
   * @param array
   * @param array
   * returns comaprison result
   */
  private function cmp1($a, $b) {
    return strcmp($b["s_admin"], $a["s_admin"]);
  }

  /**
   * Compare strings
   * @param array
   * @param array
   * returns comaprison result
   */
  private function cmp2($a, $b) {
    return strcmp($b["client_channel_group_id"], $a["client_channel_group_id"]);
  }
}