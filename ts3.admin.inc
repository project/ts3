<?php
/**
 * @file
 * Administration page functions for the TeamSpeak 3 module.
 */

/**
 * Main admin page.
 *
 * This is mainly a selection of servers so you can choose which server to
 * work on.
 *
 * @return string
 *   String of HTML content.
 */
function ts3_admin() {
  global $user;
  $content = l(t('Add Server'), 'admin/settings/ts3/add') . '<br />';
  $servers = ts3_getserver();
  if (!empty($servers)) {
    foreach ($servers as $server) {
      if (user_access('admin ts3') || ($server['uid'] == $user->uid) || user_access('edit server') || user_access('delete server')) {
        $row = array();
        $row[] = array('data' => check_plain($server['name']));
        $row[] = array('data' => check_plain($server['ip']));
        $row[] = array('data' => check_plain($server['enabled']));
        if (user_access('admin ts3') || (($server['uid'] == $user->uid) && user_access('edit own server')) || user_access('edit server')) {
          $row[] = array('data' => l(t('Edit'), sprintf('admin/settings/ts3/edit/%d', $server['ts3id'])));
        }
        else {
          $row[] = array('data' => t('no edit rights'));
        }
        if (user_access('admin ts3') || (($server['uid'] == $user->uid) && user_access('delete own server')) || user_access('delete server')) {
          $row[] = array('data' => l(t('Delete'), sprintf('admin/settings/ts3/delete/%d', $server['ts3id'])));
        }
        else {
          $row[] = array('data' => t('no edit rights'));
        }
        if (user_access('admin ts3')) {
          if (variable_get('ts3_default', 'no default setup') == $server['ts3id']) {
            $row[] = array('data' => 'Default');
          }
          else {
            $row[] = array('data' => l(t('Make Default'), sprintf('admin/settings/ts3/default/%d', $server['ts3id'])));
          }
        }
        else {
          $row[] = array('data' => t('no rights to change the default server'));
        }
        $rows = array('data' => $row); // Note the difference between $row and $rows.
        unset($row);
      }
    }
    $content .= theme('table', 'Servers', $rows, array('border' => '0', 'align' => 'center', 'cellspacing' => '20', 'width' => '100'));
  }
  return $content;
}

/**
 * Admin form to add or edit a TeamSpeak server.
 *
 * @param array $form_state
 *   Array of the form status and values.
 * @param $id
 *   The ID to edit.
 *
 * @return array
 *   Array of form data.
 */
function ts3_adminform(&$form_state, $id = NULL) {
  $form['#redirect'] = 'admin/settings/ts3';

  // Test basic permissions. No need to go further if none of these are true.
  if (!(user_access('admin ts3') || user_access('edit own server') || user_access('edit server') || user_access('add server'))) {
    drupal_set_message(t('You do not have permission to edit servers.'));
    return $form;
  }

  // Get global $user variable
  global $user;
  // Set up array of server values, for use as default values.
  foreach (array('ts3id', 'uid', 'ip', 'name', 'port', 't_port', 'sid',
                'cutname', 'cutchannel', 'queryusername', 'querypassword',
                'enabled') as $val) {
    $default_values[$val] = '';
  }

  if (!empty($id) && is_numeric($id)) {
    $server = ts3_getserver($id);

    // Halt if ts3_getserver() did not return anything useful
    if (empty($server)) {
      drupal_set_message(t('The requested server was not found. Please try another server id.'));
      return $form;
    }

    $server = $server[$id];
    if (!(user_access('admin ts3') || (($server['uid'] == $user->uid) && user_access('edit own server')) || user_access('edit server'))) {
      drupal_set_message(t('You do not have permission to edit this server.'));
      return $form;
    }
    else {
      $form['ts3id'] = array(
        '#type' => 'hidden',
        '#value' => $server['ts3id'],
      );
    }
  }
  elseif (user_access('admin ts3') || user_access('add server')) {
    // This looks hackish. We should probably remove this and check if ts3id
    // is empty instead.
    $form['ts3id'] = array(
      '#type' => 'hidden',
      '#value' => 'new',
    );
  }
  else {
    drupal_set_message(t('You do not have permission to add a server.'));
  }

  // Populate default values
  foreach ($default_values as $val) {
    if (!empty($server[$val])) {
      $default_values[$val] = $server[$val];
    }
  }
  if (empty($default_values['uid'])) {
    $default_values['uid'] = $user->uid;
  }

  // Populate the form
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $default_values['uid'],
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Server name'),
    '#size' => 25,
    '#default_value' => $default_values['name'],
    '#maxlength' => 255,
    '#description' => t('A name to easily identify your TeamSpeak server'),
  );
  $form['ip'] = array(
    '#type' => 'textfield',
    '#title' => t('TeamSpeak IP address'),
    '#size' => 20,
    '#default_value' => $default_values['ip'],
    '#maxlength' => 20,
    '#description' => t('IP address of your TeamSpeak server'),
  );
  $form['port'] = array(
    '#type' => 'textfield',
    '#title' => t('TeamSpeak port'),
    '#size' => 8,
    '#default_value' => $default_values['port'],
    '#maxlength' => 8,
    '#description' => t('Default is 9987'),
  );
  $form['t_port'] = array(
    '#type' => 'textfield',
    '#title' => t('TeamSpeak telnet port'),
    '#size' => 8,
    '#default_value' => $default_values['t_port'],
    '#maxlength' => 8,
    '#description' => t('Default is 10011'),
  );
  $form['sid'] = array(
    '#type' => 'textfield',
    '#title' => t('TeamSpeak virtual server ID'),
    '#size' => 8,
    '#default_value' => $default_values['sid'],
    '#maxlength' => 8,
  );
  $form['cutname'] = array(
    '#type' => 'textfield',
    '#title' => t('What is the max length you would like usernames to be'),
    '#size' => 8,
    '#default_value' => $default_values['cutname'],
    '#maxlength' => 8,
  );
  $form['cutchannel'] = array(
    '#type' => 'textfield',
    '#title' => t('What is the max length you would like channels to be'),
    '#size' => 8,
    '#default_value' => $default_values['cutchannel'],
    '#maxlength' => 8,
  );
  $form['queryname'] = array(
    '#type' => 'textfield',
    '#title' => t('Query username'),
    '#size' => 30,
    '#default_value' => $default_values['queryusername'],
    '#maxlength' => 255,
  );
  $form['querypassword'] = array(
    '#type' => 'textfield',
    '#title' => t('Query password'),
    '#size' => 20,
    '#default_value' => $default_values['querypassword'],
    '#maxlength' => 20,
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $default_values['enabled'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit call back for for ts3_adminform().
 *
 * Write the data to the database.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   Contains the values of the submitted form.
 */
function ts3_adminform_submit($form, &$form_values) {
  $record['ip'] = $form_values['values']['ip'];
  $record['port'] = $form_values['values']['port'];
  $record['t_port'] = $form_values['values']['t_port'];
  $record['sid'] = $form_values['values']['sid'];
  $record['cutname'] = $form_values['values']['cutname'];
  $record['cutchannel'] = $form_values['values']['cutchannel'];
  $record['queryusername'] = $form_values['values']['queryname'];
  $record['querypassword'] = $form_values['values']['querypassword'];
  $record['uid'] = $form_values['values']['uid'];
  $record['name'] = $form_values['values']['name'];
  $record['enabled'] = $form_values['values']['enabled'];
  if ($form_values['values']['ts3id'] == 'new') {
    return drupal_write_record('ts3', $record);;
  }
  else {
    $record['ts3id'] = $form_values['values']['ts3id'];
    return drupal_write_record('ts3', $record, 'ts3id');
  }
}

/**
 * Delete a server.
 *
 * @param array $form_state
 *   Array of the form status and values.
 * @param int $id
 *   The ID of the server to be deleted.
 *
 * @return array
 *   A $form array.
 */
function ts3_deleteserver(&$form_state, $id) {
  if (user_access('admin ts3') || (($server['uid'] == $user->uid) && user_access('delete own server')) || user_access('delete server')) {
    $form['ts3id'] = array(
      '#type' => 'hidden',
      '#value' => $server['ts3id'],
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
    $form['cancel'] = array(
      '#type' => 'markup',
      '#value' => l(t('Cancel'), 'admin/settings/ts3'),
    );
  }
  else {
    $content = t('You do not have permission to delete this server.');
    $form['markup'] = array(
      '#type' => 'markup',
      '#value' => $content,
    );
    drupal_set_message($content);
  }
  return $form;
}

/**
 * Submit call back for for ts3_deleteserver().
 *
 * Deletes a server from database.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   Contains the values of the submitted form.
 */
function ts3_deleteserver_submit($form, &$form_values) {
  // Check to see if it was the default server and delete variable if so.
  if (user_access('admin ts3') || (($server['uid'] == $user->uid) && user_access('delete own server')) || user_access('delete server')) {
    db_query('DELETE FROM {ts3} WHERE ts3id = %d', $form_values['values']['ts3id']);
    if (variable_get('ts3_default', 'not set') == $form_values['values']['ts3id']) {
      variable_del('ts3_default');
    }
  }
  else {
    drupal_set_message(t('You do not have permission to delete this server.'));
  }
}

/**
 * Make a server the default one.
 *
 * @param array $form_state
 *   Array of the form status and values.
 * @param int $id
 *   The ID of the server you want to make default.
 *
 * @return array
 *   A $form array.
 */
function ts3_defaultserver(&$form_state, $id) {
  if (user_access('admin ts3')) {
    $form['#redirect'] = 'admin/settings/ts3';
    $form['ts3id'] = array(
      '#type' => 'hidden',
      '#value' => $id,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Make default'),
    );
    $form['cancel'] = array(
      '#type' => 'markup',
      '#value' => l(t('Cancel'), 'admin/settings/ts3'),
    );
  }
  else {
    $content = t('You do not have permission to make this server the default.');
    drupal_set_message(check_plain($content));
    $form['markup'] = array(
      '#type' => 'markup',
      '#value' => $content,
    );
  }
  return $form;
}

/**
 * Submit call back for for ts3_defaultserver().
 *
 * Changes the default server.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   Contains the values of the submitted form.
 */
function ts3_defaultserver_submit($form, &$form_values) {
  if (user_access('admin ts3')) {
    variable_set('ts3_default', $form_values['values']['ts3id']);
  }
  else {
    drupal_set_message(t('You do not have permission to make this server the default.'));
  }
}
